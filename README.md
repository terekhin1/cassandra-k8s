Role Name
=========

install apache-cassandra cluster on k8s 

Requirements
------------

cassandra-k8s

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

# ansible-playbook -i inventory playbook.yml

    - hosts: "{{ groups ['k8s-master-group'] | random }}"

      roles:
         - { role: cassandra-k8s, become: yes, tags: cassandra }

License
-------

BSD


